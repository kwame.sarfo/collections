import java.util.Random;

public class InsertionSort {

    private Integer[] sortedArr;

    public InsertionSort()
    {
        this.sortedArr = new Integer[6];
        this.insertionSorter();
    }

    public void insertionSorter()
    {
        Random random = new Random();
        int randomNumber = random.nextInt(1,49);
        while (randomNumber > 43)
        {
            randomNumber = random.nextInt(1,49);
        }

        sortedArr[0] = randomNumber;



        for(int iter=0; iter < sortedArr.length-1; ++iter)
        {
            int loopRandomNumber = random.nextInt(1,49);
            if(sortedArr[iter] <= loopRandomNumber)
            {
                sortedArr[iter+1] = loopRandomNumber;
            }
            else if(iter > 0)
            {
                iter--;
            }
            else if(iter == 0)
            {
                iter=-1;
            }
        }

    }

    public Integer[] getSortedArr() {
        return sortedArr;
    }
}
