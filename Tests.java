import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;


public class Tests {
    @Test
    public void checkArrInitialization()
    {
        Arr intArray = new Arr();

        Integer[] expectedOut = {1,2,3};
        assertArrayEquals(expectedOut, intArray.myInts);

    }

    @Test
    public void noDuplicatesChecker()
    {
        boolean thereIsADuplicate = false;
        NoDuplicateArr nodups = new NoDuplicateArr();
        Arrays.sort(nodups.noDuplicates);
        nodups.printAll();
        for(int iter=0; iter < nodups.noDuplicates.length-1; iter++)
        {
            if(nodups.noDuplicates[iter] == nodups.noDuplicates[iter+1])
            {
                thereIsADuplicate = true;
                break;
            }
        }
        assertFalse(thereIsADuplicate);
    }

    @Test
    public void insertSorter()
    {
        InsertionSort iS = new InsertionSort();
        Integer[] sortedArr = iS.getSortedArr();

        boolean sorted = true;
        for(int iter=0; iter<sortedArr.length-1; iter++)
        {
            if(sortedArr[iter] > sortedArr[iter+1])
            {
                sorted = false;
                assertFalse(sorted);
                return;
            }
        }
        assertTrue(sorted);
    }



}
