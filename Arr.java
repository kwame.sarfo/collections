import java.util.Arrays;

public class Arr {
    protected Integer[] myInts = new Integer[3];

    public Arr()
    {
        for(int iter = 0; iter<myInts.length; iter++)
        {
            myInts[iter] = iter+1;
        }
    }

    public String printAll()
    {
        return Arrays.toString(myInts);
    }
}
