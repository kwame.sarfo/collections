import java.util.Arrays;
import java.util.Random;

public class NoDuplicateArr {
    protected Integer[] noDuplicates;

    public NoDuplicateArr()
    {
        noDuplicates = new Integer[6];
        this.arrLoader();
    }

    public void arrLoader()
    {
        Random random = new Random();
        int iter = 0;
        while(iter < 6)
        {
            int randomInt = random.nextInt(1,49);
            if(Arrays.asList(this.noDuplicates).contains(randomInt) == false)
            {
                noDuplicates[iter] = randomInt;
                iter++;
            }
        }
    }


    public Integer[] printAll()
    {
        return noDuplicates;
    }
}
